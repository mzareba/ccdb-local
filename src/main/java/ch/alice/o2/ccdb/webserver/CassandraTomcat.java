package ch.alice.o2.ccdb.webserver;

import javax.servlet.ServletException;

import ch.alice.o2.ccdb.servlets.cassandra.CassandraBacked;
import ch.alice.o2.ccdb.servlets.cassandra.CassandraBrowse;
import ch.alice.o2.ccdb.servlets.cassandra.CassandraDownload;
import ch.alice.o2.ccdb.servlets.cassandra.CassandraTruncate;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.Wrapper;

import ch.alice.o2.ccdb.servlets.MonitorServlet;

public class CassandraTomcat {

    /**
     * @param args
     * @throws ServletException
     */
    public static void main(final String[] args) throws ServletException {
        EmbeddedTomcat tomcat;

        try {
            tomcat = new EmbeddedTomcat("*");
        }
        catch (final ServletException se) {
            System.err.println("Cannot create the Tomcat server: " + se.getMessage());
            return;
        }

        tomcat.addServlet(CassandraDownload.class.getName(), "/download/*");
        final Wrapper browser = tomcat.addServlet(CassandraBrowse.class.getName(), "/browse/*");
        browser.addMapping("/latest/*");

        tomcat.addServlet(CassandraBacked.class.getName(), "/*");
        tomcat.addServlet(CassandraTruncate.class.getName(), "/truncate/*");
        tomcat.addServlet(MonitorServlet.class.getName(), "/monitor/*");

        // Start the server
        try {
            tomcat.start();
        }
        catch (final LifecycleException le) {
            System.err.println("Cannot start the Tomcat server: " + le.getMessage());
            return;
        }

        if (tomcat.debugLevel >= 1)
            System.err.println("Ready to accept HTTP calls on " + tomcat.address + ":" + tomcat.getPort());

        tomcat.blockWaiting();
    }
}
