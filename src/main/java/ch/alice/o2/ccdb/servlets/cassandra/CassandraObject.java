package ch.alice.o2.ccdb.servlets.cassandra;

import alien.catalogue.GUID;
import alien.catalogue.GUIDUtils;
import alien.monitoring.Timing;
import ch.alice.o2.ccdb.RequestParser;
import ch.alice.o2.ccdb.servlets.common.DbObject;
import ch.alice.o2.ccdb.servlets.SQLBacked;
import com.datastax.dse.driver.api.core.DseSession;
import com.datastax.dse.driver.api.core.data.time.DateRange;
import com.datastax.dse.driver.api.core.data.time.DateRangeBound;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.cql.Row;
import lazyj.StringFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.ParseException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Representation of CCDB object in Cassandra with db interactions
 *
 * @author mzareba
 * @since 2019-09-05
 */
public class CassandraObject extends DbObject
{
    public DateRange validity;
    private static final DseSession session = DseSession.builder().withKeyspace("ccdb").build();

    public static DseSession getDB()
    {
        return session;
    }

    /**
     * Create an empty object
     *
     * @param path object path
     */
    public CassandraObject(String path)
    {
        super(path);
    }

    /**
     * Create a new object from a request
     *
     * @param request
     * @param path    object path
     */
    public CassandraObject(HttpServletRequest request, String path)
    {
        super(request, path);
    }

    /**
     * Create object from database row
     *
     * @param row database row
     */
    public CassandraObject(final Row row)
    {
        super(row.getLong("createtime"), (UUID) row.getObject("id"));

        try
        {
            validity = row.get("validity", DateRange.class);

            DateRangeBound validFromTimestamp = validity.getLowerBound();
            validFrom = validFromTimestamp.getTimestamp().toInstant().toEpochMilli();
            validUntil = validity.getUpperBound().orElse(validFromTimestamp).getTimestamp()
                            .toInstant().toEpochMilli();

            size = row.getLong("size");
            md5 = row.getString("md5");
            initialValidity = row.getLong("initialvalidity");
            fileName = row.getString("filename");
            contentType = getContentType(row.getInt("contenttype"));
            uploadedFrom = row.getInetAddress("uploadedfrom").toString().replace("/", "");

            pathId = row.getInt("pathId");
            metadata = row.getMap("metadata", Integer.class, String.class);
            replicas = row.getSet("replicas", Integer.class);

            existing = true;
        }
        catch (@SuppressWarnings("unused") final Throwable t)
        {
        }
    }

    public boolean save(final HttpServletRequest request)
    {
        if (!existing || tainted)
        {
            //            if (request != null && existing) setProperty("UpdatedFrom", request.getRemoteHost());

            if (pathId == null)
                pathId = getPathId(path, true);


            try
            {
                DseSession db = getDB();
                final StringBuilder sb = new StringBuilder();

                String replicaArray = null;

                if (replicas.size() > 0)
                {
                    sb.setLength(0);
                    sb.append("{");

                    for (final Integer replica : replicas)
                    {
                        if (sb.length() > 2)
                            sb.append(',');
                        sb.append(replica);
                    }

                    sb.append('}');

                    replicaArray = sb.toString();
                }

                lastModified = System.currentTimeMillis();
                recalculateValidity();

                String metadataAsString = "{" + metadata.entrySet().stream()
                                .map(e -> e.getKey().toString() + ": '" + e.getValue() + "'")
                                .collect(
                                                Collectors.joining(", ")) + "}";
                if (existing)
                {
                    db.execute(String.format("UPDATE ccdb SET validity = '%s', "
                                                    + "replicas = %s, "
                                                    + "size = %s, "
                                                    + "md5 = '%s', "
                                                    + "filename = '%s', "
                                                    + "contenttype = %s, "
                                                    + "uploadedfrom = '%s', "
                                                    + "initialvalidity = %s, "
                                                    + "metadata = %s, "
                                                    + "lastmodified = %s "
                                                    + "WHERE id = %s and createtime = %s and pathId = %s;",
                                    validity, replicaArray, size, md5, fileName, getContentTypeId(contentType, true),
                                    uploadedFrom, initialValidity, metadataAsString, lastModified,
                                    id, createTime, pathId));
                    existing = true;
                    tainted = false;
                    return true;
                }
                else
                {
                    initialValidity = validUntil;
                    db.execute(String.format("INSERT INTO ccdb(id, pathid, validity, createtime, "
                                                    + "replicas, size, md5, filename, contenttype, uploadedfrom, "
                                                    + "initialvalidity, metadata, lastmodified) "
                                                    + "values (%s, %s, '%s', %s, %s, %s, '%s', '%s', %s, '%s', %s, %s, %s);",
                                    id, pathId, validity, createTime, replicaArray, size, md5,
                                    fileName, getContentTypeId(contentType, true),
                                    uploadedFrom, initialValidity, metadataAsString, lastModified));
                    existing = true;
                    tainted = false;

                    db.execute(String
                                    .format("UPDATE ccdb_stats SET object_count = object_count + 1, object_size = object_size + %s WHERE pathid = %s;",
                                                    size, pathId));
                    return true;
                }
            } catch (Exception ex) {}
        }
        return false;
    }

    /**
     * Retrieve from the database the only object that has this object ID
     *
     * @param id the requested ID. Cannot be <code>null</code>.
     * @return the object with this ID, if it exists. Or <code>null</code> if not.
     */
    public static final CassandraObject getObject(final UUID id)
    {
        try (Timing t = new Timing(monitor, "getObject_ms"))
        {
            if (id == null)
                return null;

            try
            {
                DseSession db = getDB();
                ResultSet resultSet =
                                db.execute(String.format("SELECT * FROM ccdb WHERE id = %s", id));
                return new CassandraObject(resultSet.one());
            } catch (Exception ex) {
                return null;
            }
        }
    }

    private static synchronized String getContentType(final Integer contentTypeId)
    {
        String value = CONTENTTYPE_REVERSE.get(contentTypeId);

        if (value != null)
            return value;

        try {
            DseSession db = getDB();
            ResultSet resultSet = db.execute(String
                            .format("SELECT contenttype from ccdb_contenttype where id = %s",
                                            contentTypeId));
            value = resultSet.one().getString(0);

            CONTENTTYPE.put(value, contentTypeId);
            CONTENTTYPE_REVERSE.put(contentTypeId, value);
        } catch (Exception ex) {}

        return value;
    }

    private static synchronized Integer getContentTypeId(final String contentType,
                    final boolean createIfNotExist)
    {
        Integer value = CONTENTTYPE.get(contentType);

        if (value != null)
            return value;

        try {
            DseSession db = getDB();
            ResultSet resultSet = db.execute(String
                            .format("SELECT id FROM ccdb_contenttype WHERE contenttype = '%s' ALLOW FILTERING",
                                            contentType));
            Row result = resultSet.one();

            if (result != null)
            {
                value = result.getInt(0);
                CONTENTTYPE.put(contentType, value);
                CONTENTTYPE_REVERSE.put(value, contentType);
                return value;
            }

            if (createIfNotExist)
            {
                Row identifierRow = db.execute("SELECT MAX(id) from ccdb_contenttype").one();
                value = identifierRow != null ? identifierRow.getInt(0) + 1 : 1;
                db.execute(String
                                .format("INSERT INTO ccdb_contenttype (id, contenttype) values (%s, '%s');",
                                                value, contentType));
                CONTENTTYPE.put(contentType, value);
                CONTENTTYPE_REVERSE.put(value, contentType);
                return value;
            }
        } catch (Exception ex) {
            return null;
        }

        return null;
    }

    public static synchronized String getMetadataString(final Integer metadataId)
    {
        String value = METADATA_REVERSE.get(metadataId);

        if (value != null)
            return value;

        try
        {
            DseSession db = getDB();
            ResultSet resultSet = db.execute(String
                            .format("SELECT metadatakey FROM ccdb_metadatakey WHERE metadataid = %s",
                                            metadataId));
            Row result = resultSet.one();

            if (result != null)
            {
                value = result.getString(0);

                METADATA.put(value, metadataId);
                METADATA_REVERSE.put(metadataId, value);
            }
        } catch (Exception ex) {}
        return value;
    }

    private static synchronized Integer getMetadataId(final String metadataKey,
                    final boolean createIfNotExists)
    {
        Integer value = METADATA.get(metadataKey);

        if (value != null)
            return value;

        try {
            DseSession db = getDB();
            ResultSet resultSet = db.execute(String
                            .format("SELECT metadataid FROM ccdb_metadata WHERE metadatakey = '%s' ALLOW FILTERING",
                                            metadataKey));
            Row result = resultSet.one();

            if (result != null)
            {
                value = result.getInt(0);
                METADATA.put(metadataKey, value);
                METADATA_REVERSE.put(value, metadataKey);
                return value;
            }

            if (createIfNotExists)
            {
                Row identifierRow = db.execute("SELECT MAX(metadataid) from ccdb_metadata").one();
                value = identifierRow != null ? identifierRow.getInt(0) + 1 : 1;
                db.execute(String
                                .format("INSERT INTO ccdb_metadata (metadataid, metadatakey) values (%s, '%s');",
                                                value, metadataKey));
                CONTENTTYPE.put(metadataKey, value);
                CONTENTTYPE_REVERSE.put(value, metadataKey);
                return value;
            }
        } catch (Exception ex) {}

        return null;
    }

    private static synchronized String getPath(final Integer pathId)
    {
        String value = PATHS_REVERSE.get(pathId);

        if (value != null)
            return value;

        try {
            DseSession db = getDB();
            ResultSet resultSet = db.execute(String
                            .format("SELECT path FROM ccdb_paths WHERE pathid = %s", pathId));
            Row result = resultSet.one();

            if (result != null)
            {
                value = result.getString(0);

                PATHS.put(value, pathId);
                PATHS_REVERSE.put(pathId, value);
            }
        } catch (Exception ex) {}

        return value;
    }

    private static synchronized Integer getPathId(final String path,
                    final boolean createIfNotExists)
    {
        Integer value = PATHS.get(path);

        if (value != null)
            return value;

        try
        {
            DseSession db = getDB();
            ResultSet resultSet = db.execute(String
                            .format("SELECT pathid FROM ccdb_paths WHERE path = '%s'", path));
            Row result = resultSet.one();

            if (result != null)
            {
                value = result.getInt(0);

                PATHS.put(path, value);
                PATHS_REVERSE.put(value, path);
                return value;
            }

            if (createIfNotExists)
            {
                Row identifierRow = db.execute("SELECT MAX(pathid) from ccdb_paths").one();
                value = identifierRow != null ? identifierRow.getInt(0) + 1 : 1;
                db.execute(String.format("INSERT INTO ccdb_paths (pathid, path) values (%s, '%s');",
                                value, path));
                CONTENTTYPE.put(path, value);
                CONTENTTYPE_REVERSE.put(value, path);
                return value;
            }
        } catch (Exception ex) {}

        return null;
    }

    private static List<Integer> getPathIds(final String pathPattern)
    {
        final List<Integer> ret = new ArrayList<>();

        try {
            DseSession db = getDB();
            ResultSet resultSet = db.execute(String
                            .format("SELECT pathid FROM ccdb_paths WHERE path LIKE '%s'",
                                            pathPattern));
            resultSet.forEach(row -> ret.add(row.getInt(0)));
        } catch (Exception ex) {}

        return ret;
    }

    static List<Integer> getPathIdsWithPatternFallback(final RequestParser parser)
    {
        final Integer exactPathId = parser.wildcardMatching ? null : getPathId(parser.path, false);

        final List<Integer> pathIds;

        if (exactPathId != null)
            pathIds = Arrays.asList(exactPathId);
        else
        {
            if (parser.path != null && (parser.path.contains("*") || parser.path.contains("%")))
            {
                pathIds = getPathIds(parser.path.replace("*", "%"));

                parser.wildcardMatching = true;

                if (pathIds == null || pathIds.size() == 0)
                    return null;
            }
            else
            {
                return null;
            }
        }
        return pathIds;
    }

    /**
     * @return the full path of this object
     */
    public String getPath()
    {
        if (path == null)
            path = getPath(pathId);

        return path;
    }

    public boolean delete()
    {
        if (existing)
        {
            try
            {
                DseSession db = getDB();
                db.execute(String.format("DELETE FROM ccdb where id = %s AND pathid = %s and createtime = %s", id, pathId, createTime));
                db.execute(String
                                .format("UPDATE ccdb_stats SET object_count = object_count - 1, object_size = object_size - %s WHERE pathid = %s;",
                                                size, pathId));
                return true;
            } catch (Exception ex) {}
        }
        return false;
    }

    /**
     * @return the metadata keys
     */
    public Set<String> getPropertiesKeys()
    {
        final Set<String> ret = new HashSet<>(metadata.size());

        for (final Integer metadataId : metadata.keySet())
            ret.add(getMetadataString(metadataId));

        return ret;
    }

    /**
     * Set a metadata field of this object. {@link #save(HttpServletRequest)} should be called afterwards to actually flush
     * this change to the persistent store.
     *
     * @param key
     * @param value
     */
    public void setProperty(final String key, final String value)
    {
        final Integer keyID = getMetadataId(key, true);

        if (value == null)
        {
            final String oldValue = metadata.remove(keyID);

            tainted = tainted || oldValue != null;
        }
        else
        {
            final String oldValue = metadata.put(keyID, value);

            tainted = tainted || !value.equals(oldValue);
        }
    }

    /**
     * Get the directory on the local filesystem (starting from the directory structure under {@link CassandraBacked#basePath}) where this file could be located. Optionally creates the directory structure
     * to it, for when the files have to be uploaded.
     *
     * @param createIfMissing create the directory structure. Set this to <code>true</code> only from upload methods, to <code>false</code> on read queries
     * @return the folder, if it exists or (if indicated so) could be created. Or <code>null</code> if any problem.
     */
    public File getLocalFolder(final boolean createIfMissing)
    {
        final File folder = new File(SQLBacked.basePath, getFolder());

        if (!folder.exists() && createIfMissing)
            if (!folder.mkdirs())
                return null;

        if (folder.exists() && folder.isDirectory())
            return folder;

        return null;
    }

    /**
     * Get the local file that is a representation of this object.
     *
     * @param createIfMissing Whether or not this is a write operation. In this case all intermediate folders are created (if possible). Pass <code>false</code> for all read-only queries.
     * @return the local file for this object ID. For uploads the folders are created but not the end file. For read queries the entire structure must exist and the file has to have the same size as
     * the database record. Will return <code>null</code> if the local file doesn't exist and/or could not be created.
     */
    public File getLocalFile(final boolean createIfMissing)
    {
        final File folder = getLocalFolder(createIfMissing);

        if (folder == null)
            return null;

        final File ret = new File(folder, id.toString());

        if (createIfMissing || (ret.exists() && ret.isFile() && ret.length() == size))
            return ret;

        return null;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder();

        sb.append("ID: ").append(id.toString()).append('\n');
        sb.append("Path: ").append(getPath()).append('\n');
        sb.append("Validity: ").append(validFrom).append(" - ").append(validUntil).append(" (")
                        .append(new Date(validFrom)).append(" - ").append(new Date(validUntil))
                        .append(")\n");
        sb.append("Initial validity limit: ").append(initialValidity).append(" (")
                        .append(new Date(initialValidity)).append(")\n");
        sb.append("Created: ").append(createTime).append(" (").append(new Date(createTime))
                        .append(")\n");
        sb.append("Last modified: ").append(lastModified).append(" (")
                        .append(new Date(lastModified)).append(")\n");
        sb.append("Original file: ").append(fileName).append(", size: ").append(size)
                        .append(", md5: ").append(md5).append(", content type: ")
                        .append(contentType).append('\n');
        sb.append("Uploaded from: ").append(uploadedFrom).append('\n');

        if (metadata != null && metadata.size() > 0)
        {
            sb.append("Metadata:\n");

            for (final Map.Entry<Integer, String> entry : metadata.entrySet())
                sb.append("  ").append(getMetadataString(entry.getKey())).append(" = ")
                                .append(entry.getValue()).append('\n');
        }

        return sb.toString();
    }

    GUID toGUID()
    {
        final GUID guid = GUIDUtils.getGUID(id, true);

        if (guid.exists())
            // It should not exist in AliEn, these UUIDs are created only in CCDB's space
            return null;

        guid.size = size;
        guid.md5 = StringFactory.get(md5);
        guid.owner = StringFactory.get("ccdb");
        guid.gowner = StringFactory.get("ccdb");
        guid.perm = "755";
        guid.ctime = new Date(createTime);
        guid.expiretime = null;
        guid.type = 0;
        guid.aclId = -1;

        return guid;
    }

    /**
     * @return last modification timestamp
     */
    public long getLastModified()
    {
        try
        {
            final Integer key = getMetadataId("LastModified", false);

            if (key != null)
            {
                final String md = metadata.get(key);

                if (md != null)
                    return Long.parseLong(md);
            }
        }
        catch (@SuppressWarnings("unused") final Throwable t)
        {
        }

        return createTime;
    }

    private DateRange recalculateValidity()
    {
        try
        {
            validity = new DateRange(DateRangeBound
                            .parseLowerBound(Instant.ofEpochMilli(validFrom).toString()),
                            DateRangeBound.parseLowerBound(
                                            Instant.ofEpochMilli(validUntil).toString()));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return validity;
    }

    /**
     * @param parser
     * @return the most recent matching object
     */
    public static CassandraObject getMatchingObject(final RequestParser parser)
    {
        try (Timing t = new Timing(monitor, "getMatchingObject_ms"))
        {
            final Integer pathId = getPathId(parser.path, false);

            if (pathId == null)
                return null;

            final List<Object> arguments = new ArrayList<>();

            try
            {
                DseSession db = getDB();
                final StringBuilder q = new StringBuilder(
                                "SELECT * FROM ccdb WHERE solr_query=$${ \"q\": \"pathid:%s\"");

                arguments.add(pathId);

                if (parser.uuidConstraint != null)
                {
                    q.append(" , \"fq\": \"id: %s\"");

                    arguments.add(parser.uuidConstraint);
                }

                if (parser.startTimeSet)
                {
                    q.append(" , \"fq\": \"validity: %s\"");

                    arguments.add(new DateRange(DateRangeBound.parseLowerBound(
                                    Instant.ofEpochMilli(parser.startTime).toString()),
                                    DateRangeBound.UNBOUNDED));
                }

                if (parser.notAfter > 0)
                {
                    q.append(" ,\"fq\": \"createtime:[* TO %s]\"");

                    arguments.add(Long.valueOf(parser.notAfter));
                }


                // TODO querying for metadata is currenlty unavailable
//                if (parser.flagConstraints != null && parser.flagConstraints.size() > 0)
//                {
//                    for (final Map.Entry<String, String> constraint : parser.flagConstraints
//                                    .entrySet())
//                    {
//                        final String key = constraint.getKey();
//
//                        final Integer metadataId = getMetadataId(key, false);
//
//                        if (metadataId == null)
//                            return null;
//
//                        final String value = constraint.getValue();
//
//                        q.append(" AND metadata[%s] = '%s'");
//
//                        arguments.add(metadataId.toString());
//                        arguments.add(value);
//                    }
//                }

                q.append("}$$ LIMIT 1;");

                Row row = db.execute(String.format(q.toString(), arguments.toArray())).one();

                if (row != null)
                    return new CassandraObject(row);

                System.err.println("No object for:\n" + q + "\nand\n" + arguments + "\n");
                return null;
            }
            catch (ParseException e) {
                return null;
            }
        }
    }

    public static final Collection<CassandraObject> getAllMatchingObjects(
                    final RequestParser parser)
    {
        try (Timing t = new Timing(monitor, "getAllMatchingObjects_ms"))
        {
            final List<Integer> pathIDs = getPathIdsWithPatternFallback(parser);

            if (pathIDs == null || pathIDs.isEmpty())
                return null;

            final List<CassandraObject> ret = new ArrayList<>();

            try
            {
                DseSession db = getDB();
                for (final Integer pathId : pathIDs)
                {
                    final StringBuilder q =
                                    new StringBuilder("SELECT * FROM ccdb WHERE pathId= %s");

                    final List<Object> arguments = new ArrayList<>();

                    arguments.add(pathId);

                    if (parser.uuidConstraint != null)
                    {
                        q.append(" AND id = %s");

                        arguments.add(parser.uuidConstraint);
                    }

                    if (parser.startTimeSet)
                    {
                        q.append(" AND solr_query='validity: %s'");

                        arguments.add(new DateRange(DateRangeBound.parseLowerBound(
                                        Instant.ofEpochMilli(parser.startTime).toString()),
                                        DateRangeBound.UNBOUNDED));
                    }

                    if (parser.notAfter > 0)
                    {
                        q.append(" AND createTime <= %s");

                        arguments.add(Long.valueOf(parser.notAfter));
                    }

                    if (parser.flagConstraints != null && parser.flagConstraints.size() > 0)
                    {
                        for (final Map.Entry<String, String> constraint : parser.flagConstraints
                                        .entrySet())
                        {
                            final String key = constraint.getKey();

                            final Integer metadataId = getMetadataId(key, false);

                            if (metadataId == null)
                                return null;

                            final String value = constraint.getValue();

                            q.append(" AND metadata[%s] = '%s'");

                            arguments.add(metadataId.toString());
                            arguments.add(value);
                        }

                        q.append("ALLOW FILTERING");
                    }

                    q.append(" ORDER BY createTime DESC");

                    if (parser.latestFlag)
                        q.append(" LIMIT 1");

                    ResultSet resultSet =
                                    db.execute(String.format(q.toString(), arguments.toArray()));

                    resultSet.forEach(row -> ret.add(new CassandraObject(row)));
                }
            }
            catch (@SuppressWarnings("unused") Throwable throwable)
            {
            }
            return ret;
        }
    }
}
