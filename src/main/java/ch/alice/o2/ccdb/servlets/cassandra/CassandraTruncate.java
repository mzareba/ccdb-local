package ch.alice.o2.ccdb.servlets.cassandra;

import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.monitoring.Timing;
import ch.alice.o2.ccdb.RequestParser;
import com.datastax.dse.driver.api.core.DseSession;
import com.datastax.oss.driver.api.core.cql.Row;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
/**
 * Remove all matching objects in a single operation.
 *
 * @author mzareba
 * @since 2019-09-06
 */
@WebServlet("/truncate/*")
public class CassandraTruncate extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    private static final Monitor monitor = MonitorFactory.getMonitor(CassandraTruncate.class.getCanonicalName());

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws
                    ServletException, IOException
    {
        try (Timing t = new Timing(monitor, "TRUNCATE_ms")) {
            final RequestParser parser = new RequestParser(request, true);

            final Collection<CassandraObject> matchingObjects = CassandraObject.getAllMatchingObjects(parser);

            if (matchingObjects != null && matchingObjects.size() > 0) {
                for (final CassandraObject object : matchingObjects) {
                    if (!object.delete()) {
                        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Cannot delete " + object.id);
                        return;
                    }

                    // TODO fix when AsyncPhysicalRemoval is done
//                    AsyncPhysicalRemovalThread.queueDeletion(object);
                }

                response.setHeader("Deleted", matchingObjects.size() + " objects");

                response.sendError(HttpServletResponse.SC_NO_CONTENT);

                final List<Integer> pathIDs = CassandraObject.getPathIdsWithPatternFallback(parser);

                if (pathIDs != null)
                    try (DseSession db = CassandraObject.getDB()) {
                        for (final Integer pathID : pathIDs) {
                            Row row = db.execute(String.format("SELECT * FROM ccdb WHERE pathid=? LIMIT 1;", pathID)).one();

                            if (row != null) {
                                db.execute(String.format("DELETE FROM ccdb_paths WHERE pathid=?;", pathID));
                                CassandraObject.removePathID(pathID);

                            }
                        }
                    }
            }
            else
                response.sendError(HttpServletResponse.SC_NOT_MODIFIED);
        }
    }
}
