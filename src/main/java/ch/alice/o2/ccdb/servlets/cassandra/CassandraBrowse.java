package ch.alice.o2.ccdb.servlets.cassandra;

import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.monitoring.Timing;
import ch.alice.o2.ccdb.RequestParser;
import ch.alice.o2.ccdb.servlets.formatters.*;
import com.datastax.dse.driver.api.core.DseSession;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.cql.Row;
import lazyj.Utils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/**
 * SQL-backed implementation of CCDB. This servlet implements browsing of objects in a particular path
 *
 * @author costing
 * @since 2018-04-26
 */
@WebServlet("/browse/*")
public class CassandraBrowse extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    private static final Monitor monitor =
                    MonitorFactory.getMonitor(CassandraBrowse.class.getCanonicalName());

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
                    throws ServletException, IOException
    {
        try (Timing t = new Timing(monitor, "GET_ms"))
        {
            // list of objects matching the request
            // URL parameters are:
            // task name / detector name [ / time [ / UUID ] | [ / query string]* ]
            // if time is missing - get the last available time
            // query string example: "quality=2"

            final RequestParser parser = new RequestParser(request, true);

            /*
             * if (!parser.ok) {
             * printUsage(request, response);
             * return;
             * }
             */

            final Collection<CassandraObject> matchingObjects =
                            CassandraObject.getAllMatchingObjects(parser);

            String sContentType;
            SQLFormatter formatter = null;

            String sAccept = request.getParameter("Accept");

            if ((sAccept == null) || (sAccept.length() == 0))
                sAccept = request.getHeader("Accept");

            if ((sAccept == null || sAccept.length() == 0))
                sAccept = "text/plain";

            sAccept = sAccept.toLowerCase();

            if ((sAccept.indexOf("application/json") >= 0) || (sAccept.indexOf("text/json") >= 0))
            {
                sContentType = "application/json";
                formatter = new JSONFormatter();
            }
            else if (sAccept.indexOf("text/html") >= 0)
            {
                sContentType = "text/html";
                formatter = new HTMLFormatter();
            }
            else if (sAccept.indexOf("text/xml") >= 0)
            {
                sContentType = "text/xml";
                formatter = new XMLFormatter();
            }
            else
            {
                sContentType = "text/plain";
                formatter = new TextFormatter();
            }

            response.setContentType(sContentType);

            final boolean sizeReport = Utils.stringToBool(request.getParameter("report"), false);

            formatter.setExtendedReport(sizeReport);

            try (PrintWriter pw = response.getWriter())
            {
                formatter.start(pw);

                formatter.header(pw);

                boolean first = true;

                if (matchingObjects != null)
                    for (final CassandraObject object : matchingObjects)
                    {
                        if (first)
                            first = false;
                        else
                            formatter.middle(pw);

                        formatter.format(pw, object);
                    }

                formatter.footer(pw);

                if (!parser.wildcardMatching)
                {
                    // It is not clear which subfolders to list in case of a wildcard matching of objects. As the full hierarchy was included in the search there is no point in showing them, so just skip
                    // this section.
                    formatter.subfoldersListingHeader(pw);

                    long thisFolderCount = 0;
                    long thisFolderSize = 0;

                    try
                    {
                        String prefix = "";

                        String suffix = "";

                        ResultSet resultSet;
                        if (parser.path == null || parser.path.length() == 0)
                            resultSet =
                                            CassandraObject.getDB().execute("select split_part(path,'/',0), pathid from ccdb_paths;");
                        else
                        {
                            int cnt = 0;

                            for (final char c : parser.path.toCharArray())
                                if (c == '/')
                                    cnt++;

                            resultSet = CassandraObject.getDB().execute("select split_part(path,'/'," + (cnt + 1)
                                            + "), pathid from ccdb_paths where path like '"
                                            + parser.path + "/%'");

                            prefix = parser.path + "/";
                        }

                        if (parser.startTimeSet)
                            suffix += "/" + parser.startTime;

                        if (parser.uuidConstraint != null)
                            suffix += "/" + parser.uuidConstraint;

                        for (final Map.Entry<String, String> entry : parser.flagConstraints
                                        .entrySet())
                            suffix += "/" + entry.getKey() + "=" + entry.getValue();

                        first = true;

                        Map<String, List<Integer>> pathSuffixes = new HashMap<>();
                        resultSet.forEach(row -> {
                            String pathSuff = row.getString(0);
                            if (pathSuff != "" && pathSuff != null)
                            {
                                pathSuffixes.put(pathSuff, Stream.concat(
                                                pathSuffixes.getOrDefault(
                                                                pathSuff, new ArrayList<>())
                                                                .stream(),
                                                Stream.of(row.getInt(1)))
                                                .collect(Collectors.toList()));
                            }
                        });

                        Iterator<Map.Entry<String, List<Integer>>> iterator =
                                        pathSuffixes.entrySet().iterator();

                        while (iterator.hasNext())
                        {
                            Map.Entry<String, List<Integer>> row = iterator.next();
                            if (first)
                                first = false;
                            else
                                formatter.middle(pw);

                            if (sizeReport)
                            {
                                try
                                {
                                    int identifier = CassandraObject.getDB().execute("SELECT pathid FROM ccdb_paths WHERE path='"
                                                                    + prefix + row.getKey() + "';")
                                                                    .one().getInt(0);
                                    Row rowSizeReport = CassandraObject.getDB().execute("SELECT object_count, object_size FROM ccdb_stats WHERE pathid="
                                                                    + identifier + ";").one();

                                    final long ownCount = rowSizeReport.getLong(0);
                                    final long ownSize = rowSizeReport.getLong(1);

                                    Row rowSubfolders = CassandraObject.getDB().execute("SELECT sum(object_count), sum(object_size) FROM ccdb_stats WHERE pathid IN ("
                                                                    + row.getValue().stream()
                                                                                    .map(e -> e.toString())
                                                                                    .collect(Collectors.joining(", "))
                                                                    + ");").one();

                                    final long subfoldersCount = rowSubfolders.getLong(0);
                                    final long subfoldersSize = rowSubfolders.getLong(1);

                                    formatter.subfoldersListing(pw, prefix + row.getKey(),
                                                    prefix + row.getKey() + suffix, ownCount, ownSize,
                                                    subfoldersCount, subfoldersSize);
                                }
                                catch (@SuppressWarnings("unused") Throwable throwable) {}

                            }
                            else
                                formatter.subfoldersListing(pw, prefix + row,
                                                prefix + row + suffix);
                        }

                        if (sizeReport)
                        {

                            int identifier = CassandraObject.getDB().execute("SELECT pathid FROM ccdb_paths WHERE path='"
                                            + parser.path + "';")
                                            .one().getInt(0);
                            Row rowSizeReport =
                                            CassandraObject.getDB().execute("SELECT object_count, object_size FROM ccdb_stats WHERE pathid='"
                                                            + identifier+ "';").one();

                            thisFolderCount = rowSizeReport.getLong(0);
                            thisFolderSize = rowSizeReport.getLong(1);
                        }
                    }
                    catch (@SuppressWarnings("unused") Throwable throwable) {}

                    formatter.subfoldersListingFooter(pw, thisFolderCount, thisFolderSize);
                }

                formatter.end(pw);
            }
        }
    }
}
