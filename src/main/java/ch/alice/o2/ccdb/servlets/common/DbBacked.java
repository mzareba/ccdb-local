package ch.alice.o2.ccdb.servlets.common;

import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import ch.alice.o2.ccdb.Options;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

public class DbBacked extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected static final Monitor monitor =
                    MonitorFactory.getMonitor(DbBacked.class.getCanonicalName());

    /**
     * The base path of the file repository
     */
    public static final String basePath = Options
                    .getOption("file.repository.location", System.getProperty("user.home") + System
                                    .getProperty("file.separator") + "QC");

    protected static void setAltLocationHeaders(final HttpServletResponse response,
                    final DbObject obj)
    {
        for (final Integer replica : obj.replicas)
            if (replica.intValue() == 0)
                response.addHeader("Content-Location", "/download/" + obj.id);
            else
                response.addHeader("Content-Location", obj.getAddress(replica));
    }

}
