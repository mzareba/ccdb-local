package ch.alice.o2.ccdb.servlets.common;

import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.se.SE;
import alien.se.SEUtils;
import ch.alice.o2.ccdb.Options;
import ch.alice.o2.ccdb.UUIDTools;
import lazyj.ExtProperties;
import lazyj.Format;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.util.*;

public class DbObject
{
    protected static ExtProperties config = new ExtProperties(Options.getOption("config.dir", "."),
                    Options.getOption("config.file", "config"));
    protected static final Monitor monitor =
                    MonitorFactory.getMonitor(DbObject.class.getCanonicalName());

    /**
     * Unique identifier of an object
     */
    public final UUID id;

    /**
     * Path identifier
     */
    public Integer pathId = null;

    /**
     * String representation of the path
     */
    public String path;

    /**
     * Creation time, in epoch milliseconds
     */
    public final long createTime;

    /**
     * Starting time of the validity of this object, in epoch milliseconds. Inclusive value.
     */
    public long validFrom = System.currentTimeMillis();

    /**
     * Ending time of the validity of this object, in epoch milliseconds. Exclusive value.
     */
    public long validUntil = validFrom + 1;

    /**
     * Metadata fields set for this object
     */
    public Map<Integer, String> metadata = new HashMap<>();

    /**
     * Servers holding a replica of this object
     */
    public Set<Integer> replicas = new HashSet<>();

    /**
     * Size of the object
     */
    public long size = -1;

    /**
     * MD5 checksum
     */
    public String md5 = null;

    /**
     * Initial validity of this object (might be updated but this is to account for how long it was at the beginning)
     */
    public long initialValidity = validUntil;

    /**
     * Original file name that was uploaded (will be presented with the same name to the client)
     */
    public String fileName = null;

    /**
     * Content type
     */
    public String contentType = null;

    /**
     * IP address of the client that has uploaded this object
     */
    public String uploadedFrom = null;

    /**
     * Timestamp of the last update
     */
    public long lastModified = System.currentTimeMillis();

    protected transient boolean existing = false;
    protected transient boolean tainted = false;

    protected static Map<String, Integer> PATHS = new HashMap<>();
    protected static Map<Integer, String> PATHS_REVERSE = new HashMap<>();

    protected static Map<String, Integer> METADATA = new HashMap<>();
    protected static Map<Integer, String> METADATA_REVERSE = new HashMap<>();

    protected static Map<String, Integer> CONTENTTYPE = new HashMap<>();
    protected static Map<Integer, String> CONTENTTYPE_REVERSE = new HashMap<>();

    public DbObject()
    {
        this.createTime = System.currentTimeMillis();
        this.id = UUIDTools.generateTimeUUID(createTime, null);
    }

    public DbObject(long createTime, UUID id)
    {
        this.createTime = createTime;
        this.id = id;
    }

    /**
     * Create an empty object
     *
     * @param path object path
     */
    public DbObject(final String path)
    {
        this();

        assert path != null && path.length() > 0;

        this.path = path;
    }

    /**
     * Create a new object from a request
     *
     * @param request
     * @param path    object path
     */
    public DbObject(final HttpServletRequest request, final String path)
    {
        createTime = System.currentTimeMillis();

        byte[] remoteAddress = null;

        if (request != null)
            try
            {
                final InetAddress ia = InetAddress.getByName(request.getRemoteAddr());

                remoteAddress = ia.getAddress();
            }
            catch (@SuppressWarnings("unused") final Throwable t)
            {
                // ignore
            }

        id = UUIDTools.generateTimeUUID(createTime, remoteAddress);

        assert path != null && path.length() > 0;

        this.path = path;
    }

    public boolean save(final HttpServletRequest request)
    {
        return false;
    }

    /**
     * @return the folder path to this object.
     */
    public String getFolder()
    {
        final int hash = Math.abs(id.hashCode() % 1000000);

        return hash % 100 + "/" + hash / 100;
    }

    /**
     * Return the full URL to the physical representation on this replica ID
     *
     * @param replica
     * @return full URL
     */
    public String getAddress(final Integer replica)
    {
        String pattern = config.gets("server." + replica + ".urlPattern", null);

        if (pattern == null)
        {
            if (replica.intValue() == 0)
            {
                String hostname;

                try
                {
                    hostname = InetAddress.getLocalHost().getCanonicalHostName();
                }
                catch (@SuppressWarnings("unused") final Throwable t)
                {
                    hostname = "localhost";
                }

                pattern = "http://" + hostname + ":" + Options.getIntOption("tomcat.port", 8080)
                                + "/download/UUID";
            }
            else
            {
                final SE se = SEUtils.getSE(replica.intValue());

                if (se != null)
                {
                    pattern = se.generateProtocol();
                    if (!pattern.endsWith("/"))
                        pattern += "/";

                    pattern += "PATH.ccdb";
                }
                else
                    pattern += "alien:///alice/ccdb/FOLDER/UUID";
            }

            config.set("server." + replica + ".urlPattern", pattern);
        }

        pattern = Format.replace(pattern, "UUID", id.toString());
        pattern = Format.replace(pattern, "FOLDER", getFolder());
        pattern = Format.replace(pattern, "PATH", SE.generatePath(id.toString()));

        return pattern;
    }

    /**
     * Get all URLs where replicas of this object can be retrieved from
     *
     * @return the list of URLs where the content of this object can be retrieved from
     */
    public List<String> getAddresses()
    {
        final List<String> ret = new ArrayList<>(replicas.size());

        for (final Integer replica : replicas)
            ret.add(getAddress(replica));

        return ret;
    }

    /**
     * Modify the expiration time of an object
     *
     * @param newEndTime
     * @return <code>true</code> if the value was modified
     */
    public boolean setValidityLimit(final long newEndTime)
    {
        if (newEndTime != validUntil && newEndTime > validFrom)
        {
            validUntil = newEndTime;
            tainted = true;
            return true;
        }

        return false;
    }

    public static synchronized String removePathID(final Integer pathID)
    {
        final String path = PATHS_REVERSE.remove(pathID);

        if (path != null)
            PATHS.remove(path);

        return path;
    }

}
