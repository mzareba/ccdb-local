package ch.alice.o2.ccdb.servlets.cassandra;

import alien.monitoring.Timing;
import ch.alice.o2.ccdb.RequestParser;
import ch.alice.o2.ccdb.servlets.common.DbBacked;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import static ch.alice.o2.ccdb.servlets.ServletHelper.printUsage;

/**
 * Cassandra-backed implementation of CCDB
 *
 * @author mzareba
 * @since 2019-09-03
 */
@WebServlet("/*")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 100)
public class CassandraBacked extends DbBacked
{
    @Override
    protected void doHead(final HttpServletRequest request, final HttpServletResponse response) throws
                    ServletException, IOException {
        try (Timing t = new Timing(monitor, "HEAD_ms")) {
            doGet(request, response, true);
        }
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        try (Timing t = new Timing(monitor, "GET_ms")) {
            doGet(request, response, false);
        }
    }

    static void setMD5Header(final CassandraObject obj, final HttpServletResponse response) {
        if (obj.md5 != null && !obj.md5.isEmpty())
            response.setHeader("Content-MD5", obj.md5);
    }

    static void setHeaders(final CassandraObject obj, final HttpServletResponse response) {
        response.setDateHeader("Date", System.currentTimeMillis());
        response.setHeader("Valid-Until", String.valueOf(obj.validUntil));
        response.setHeader("Valid-From", String.valueOf(obj.validFrom));

        if (obj.initialValidity != obj.validUntil)
            response.setHeader("InitialValidityLimit", String.valueOf(obj.initialValidity));

        response.setHeader("Created", String.valueOf(obj.createTime));
        response.setHeader("ETag", "\"" + obj.id + "\"");

        response.setDateHeader("Last-Modified", obj.getLastModified());

        for (final Map.Entry<Integer, String> metadataEntry : obj.metadata.entrySet()) {
            final String mdKey = CassandraObject.getMetadataString(metadataEntry.getKey());

            if (mdKey != null)
                response.setHeader(mdKey, metadataEntry.getValue());
        }
    }

    @Override
    protected void doOptions(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        try (Timing t = new Timing(monitor, "OPTIONS_ms")) {
            response.setHeader("Allow", "GET, POST, PUT, DELETE, OPTIONS");

            final RequestParser parser = new RequestParser(request);

            if (!parser.ok)
                return;

            final CassandraObject matchingObject = CassandraObject.getMatchingObject(parser);

            if (matchingObject == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "No matching objects found");
                return;
            }

            setHeaders(matchingObject, response);
        }
    }

    @Override
    protected void doDelete(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        try (Timing t = new Timing(monitor, "DELETE_ms")) {
            final RequestParser parser = new RequestParser(request);

            if (!parser.ok) {
                printUsage(request, response);
                return;
            }

            final CassandraObject matchingObject = CassandraObject.getMatchingObject(parser);

            if (matchingObject == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "No matching objects found");
                return;
            }

            if (!matchingObject.delete()) {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Could not delete the underlying record");
                return;
            }

            setHeaders(matchingObject, response);

            response.sendError(HttpServletResponse.SC_NO_CONTENT);

            // TODO fix after rewriting AsyncPhysicalRemovalThread
//            AsyncPhysicalRemovalThread.deleteReplicas(matchingObject);
        }
    }

    @Override
    protected void doPut(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        try (Timing t = new Timing(monitor, "PUT_ms")) {
            final RequestParser parser = new RequestParser(request);

            if (!parser.ok) {
                printUsage(request, response);
                return;
            }

            final CassandraObject matchingObject = CassandraObject.getMatchingObject(parser);

            if (matchingObject == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "No matching objects found");
                return;
            }

            for (final Map.Entry<String, String[]> param : request.getParameterMap().entrySet())
                if (param.getValue().length > 0)
                    matchingObject.setProperty(param.getKey(), param.getValue()[0]);

            if (parser.endTimeSet)
                matchingObject.setValidityLimit(parser.endTime);

            final boolean changed = matchingObject.save(request);

            setHeaders(matchingObject, response);

            response.setHeader("Location", matchingObject.getAddresses().iterator().next());

            if (changed)
                response.sendError(HttpServletResponse.SC_NO_CONTENT);
            else
                response.sendError(HttpServletResponse.SC_NOT_MODIFIED);
        }
    }

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        // create the given object and return the unique identifier to it
        // URL parameters are:
        // task name / detector name / start time [/ end time] [ / flag ]*
        // mime-encoded blob is the value to be stored
        // if end time is missing then it will be set to the same value as start time
        // flags are in the form "key=value"

        try (Timing t = new Timing(monitor, "POST_ms")) {
            final RequestParser parser = new RequestParser(request);

            if (!parser.ok) {
                printUsage(request, response);
                return;
            }

            final Collection<Part> parts = request.getParts();

            if (parts.size() == 0) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "POST request doesn't contain the data to upload");
                return;
            }

            if (parts.size() > 1) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "A single object can be uploaded at a time");
                return;
            }

            final Part part = parts.iterator().next();

            final CassandraObject newObject = new CassandraObject(request, parser.path);

            final File targetFile = newObject.getLocalFile(true);

            if (targetFile == null) {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Cannot create target file to perform the upload");
                return;
            }

            final MessageDigest md5;

            try {
                md5 = MessageDigest.getInstance("MD5");
            }
            catch (@SuppressWarnings("unused") final NoSuchAlgorithmException e) {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Cannot initialize the MD5 digester");
                return;
            }

            newObject.size = 0;

            try (FileOutputStream fos = new FileOutputStream(targetFile); InputStream is = part.getInputStream()) {
                final byte[] buffer = new byte[1024 * 16];

                int n;

                while ((n = is.read(buffer)) >= 0) {
                    fos.write(buffer, 0, n);
                    md5.update(buffer, 0, n);
                    newObject.size += n;
                }
            }
            catch (@SuppressWarnings("unused") final IOException ioe) {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Cannot upload the blob to the local file " + targetFile.getAbsolutePath());
                targetFile.delete();
                return;
            }

            for (final Map.Entry<String, String> constraint : parser.flagConstraints.entrySet())
                newObject.setProperty(constraint.getKey(), constraint.getValue());

            newObject.uploadedFrom = request.getRemoteHost();
            newObject.fileName = part.getSubmittedFileName();
            newObject.contentType = part.getContentType();
            newObject.md5 = String.format("%032x", new BigInteger(1, md5.digest())); // UUIDTools.getMD5(targetFile);
            newObject.setProperty("partName", part.getName());

            newObject.replicas.add(Integer.valueOf(0));

            newObject.validFrom = parser.startTime;

            newObject.setValidityLimit(parser.endTime);

            if (!newObject.save(request)) {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Cannot insert the object in the database");
                return;
            }

            // TODO Fix when async replication is done
//            AsyncReplication.queueDefaultReplication(newObject);

            setHeaders(newObject, response);

            final String location = newObject.getAddress(Integer.valueOf(0));

            response.setHeader("Location", location);
            response.setHeader("Content-Location", location);

            response.sendError(HttpServletResponse.SC_CREATED);
        }
    }

    protected static void doGet(final HttpServletRequest request, final HttpServletResponse response, final boolean head) throws IOException {
        // list of objects matching the request
        // URL parameters are:
        // task name / detector name [ / time [ / UUID ] | [ / query string]* ]
        // if time is missing - get the last available time
        // query string example: "quality=2"

        final RequestParser parser = new RequestParser(request);

        if (!parser.ok) {
            printUsage(request, response);
            return;
        }

        final CassandraObject matchingObject = CassandraObject.getMatchingObject(parser);

        if (matchingObject == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "No matching objects found");
            return;
        }

        if (parser.cachedValue != null && matchingObject.id.equals(parser.cachedValue)) {
            response.sendError(HttpServletResponse.SC_NOT_MODIFIED);
            return;
        }

        setAltLocationHeaders(response, matchingObject);

        if (matchingObject.replicas.contains(Integer.valueOf(0))) {
            CassandraDownload.download(head, matchingObject, request, response);
            return;
        }

        setHeaders(matchingObject, response);

        Iterator<String> iter = matchingObject.getAddresses().iterator();
        if (iter.hasNext())
            response.sendRedirect(iter.next());
    }
}
