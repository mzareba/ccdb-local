package ch.alice.o2.ccdb.servlets.common;

import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

public class DbDownload extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected static final Monitor monitor = MonitorFactory.getMonitor(DbDownload.class.getCanonicalName());

    protected static void copy(final RandomAccessFile input, final OutputStream output, final long count) throws IOException {
        final byte[] buffer = new byte[4096];
        int cnt = 0;

        long leftToCopy = count;

        do {
            final int toCopy = (int) Math.min(leftToCopy, buffer.length);

            cnt = input.read(buffer, 0, toCopy);

            output.write(buffer, 0, cnt);

            leftToCopy -= cnt;
        } while (leftToCopy > 0);
    }

    @Override
    protected void doDelete(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        resp.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED, "The DELETE method should use the main entry point instead of /download/, which is reserved for direct read access to the objects");
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        resp.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED, "You shouldn't try to create objects via the /download/ servlet, go to / instead");
    }

    @Override
    protected void doPut(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        resp.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED, "Object manipulation is not available via the /download/ servlet, go to / instead");
    }
}
